chrome.runtime.onMessage.addListener(function (request, sender, sendResponse) {
  if (request.action === "getSiteData") {
    const h1Element = document.querySelector("h1");
    const h1Text = h1Element ? h1Element.textContent : "";
    const currentUrl = window.location.href;
    const authorElement = document.querySelector(".author");
    const authorText = authorElement ? authorElement.textContent : "";
    const dateElement = document.querySelector(".date");
    const dateText = dateElement ? dateElement.textContent : "";
    sendResponse({ h1Text, currentUrl, authorText, dateText });
  } else if (request.action === "alertSuccess") {
    // alert("Data sent to server!");
  } else if (request.action === "alertError") {
    // alert("Error sending data to server");
  }
});
