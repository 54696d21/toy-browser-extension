document.addEventListener("DOMContentLoaded", function () {
  const sendButton = document.getElementById("sendButton");
  sendButton.addEventListener("click", function () {
    chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
      chrome.tabs.sendMessage(
        tabs[0].id,
        { action: "getSiteData" },
        function (response) {
          const { h1Text, currentUrl, authorText, dateText } = response;
          console.log("site data:", response);
          fetch("https://localhost:5000/data", {
            method: "POST",
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify({ h1Text, currentUrl, authorText, dateText }),
          })
            .then((response) => {
              if (response.ok) {
                document.body.style.backgroundColor = "#66FF99";
                chrome.tabs.sendMessage(tabs[0].id, { action: "alertSuccess" });
                setTimeout(() => window.close(), 500); // close the popup after 500ms
              } else {
                throw new Error("Network response was not ok");
              }
            })
            .catch(() => {
              document.body.style.backgroundColor = "#FFCCCB";
              chrome.tabs.sendMessage(tabs[0].id, { action: "alertError" });
            });
        }
      );
    });
  });
});
