from datetime import datetime  # import datetime module
from flask import Flask, request
import json
from flask_cors import CORS
from flask_sslify import SSLify
from flask_sqlalchemy import SQLAlchemy  # import Flask-SQLAlchemy

app = Flask(__name__)
CORS(app)
sslify = SSLify(app)

# configure the database URI
app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///data.db"
db = SQLAlchemy(app)  # create the SQLAlchemy instance with the app


class Data(db.Model):  # subclass db.Model
    id = db.Column(db.Integer, primary_key=True)
    url = db.Column(db.String)
    h1 = db.Column(db.String)
    subhead = db.Column(db.String)
    article_body = db.Column(db.String)
    author = db.Column(db.String)
    date = db.Column(db.String)
    # use db.DateTime to store the insert time
    insert_time = db.Column(db.DateTime, default=datetime.utcnow)


with app.app_context():
    db.create_all()


@app.route('/')
def hello_world():
    return 'Hello, World!'


@app.route('/data', methods=['POST'])
def handle_data():
    data = json.loads(request.data)
    print(data)
    current_url = data.get('currentUrl')
    h1_text = data.get('h1Text')
    subhead_text = data.get('subheadText')
    article_body_text = data.get('articleBodyText')
    author_text = data.get('authorText')
    date_text = data.get('dateText')

    new_data = Data(url=current_url, h1=h1_text, subhead=subhead_text,
                    article_body=article_body_text, author=author_text,
                    date=date_text)  # create a new Data object
    db.session.add(new_data)  # add it to the session
    db.session.commit()  # commit the session

    return 'Data received'


if __name__ == '__main__':
    context = ('ssl/cert.pem', 'ssl/key.pem')
    app.run(debug=True, ssl_context=context)
