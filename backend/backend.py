from flask import Flask, request
import sqlite3
import json
from flask_cors import CORS
from flask_sslify import SSLify
import datetime  # import datetime module

app = Flask(__name__)
CORS(app)
sslify = SSLify(app)


@app.route('/')
def hello_world():
    return 'Hello, World!'


@app.route('/data', methods=['POST'])
def handle_data():
    data = json.loads(request.data)
    print(data)
    current_url = data.get('currentUrl')
    h1_text = data.get('h1Text')
    subhead_text = data.get('subheadText')
    article_body_text = data.get('articleBodyText')
    author_text = data.get('authorText')
    date_text = data.get('dateText')
    insert_time = datetime.datetime.now()  # get the current time and date

    conn = sqlite3.connect('data.db')
    c = conn.cursor()
    # add a new column for insert_time
    c.execute('CREATE TABLE IF NOT EXISTS data (url TEXT, h1 TEXT, subhead TEXT, article_body TEXT, author TEXT, date TEXT, insert_time TEXT)')
    c.execute('INSERT INTO data (url, h1, subhead, article_body, author, date, insert_time) VALUES (?, ?, ?, ?, ?, ?, ?)',
              (current_url, h1_text, subhead_text, article_body_text, author_text, date_text, insert_time))  # insert the insert_time value
    conn.commit()
    conn.close()

    return 'Data received'


if __name__ == '__main__':
    context = ('ssl/cert.pem', 'ssl/key.pem')
    app.run(debug=True, ssl_context=context)
