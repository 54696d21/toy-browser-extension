import sqlite3
import argparse
import tabulate

parser = argparse.ArgumentParser(
    description="A simple sqlite db viewer in terminal")
parser.add_argument("filename", help="The name of the database file")
parser.add_argument("-q", "--quiet", action="store_true",
                    help="Do not print table names and columns")
args = parser.parse_args()

conn = sqlite3.connect(args.filename)
cur = conn.cursor()
cur.execute("SELECT name FROM sqlite_master WHERE type='table';")
tables = cur.fetchall()

if not args.quiet:
    # Print the table names
    print("Tables in the database:")
    for table in tables:
        print(table[0])

# If there is only one table in the database, display it without asking
if len(tables) == 1:
    choice = tables[0][0]
else:
    # Ask the user to choose a table to view
    choice = input("Enter the name of the table you want to view: ")

# Execute a query to get all the column names and data types of the chosen table
cur.execute(f"PRAGMA table_info({choice});")
# Fetch the results as a list of tuples
columns = cur.fetchall()

if not args.quiet:
    # Print the column names and data types
    print(f"Columns in {choice} table:")
    for column in columns:
        print(column[1], column[2])

# Execute a query to get all the rows of the chosen table
cur.execute(f"SELECT * FROM {choice};")
rows = cur.fetchall()

# Print the rows using tabulate
print(f"Rows in {choice} table:")
print(tabulate.tabulate(rows, headers=[column[1] for column in columns]))

# Close the connection
conn.close()
